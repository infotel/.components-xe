The location where the DX Designtime Loader is looking for the skin packages has changed again !

For Delphi DX10.3 Rio now all skin packages (=dxSkin*.bpl) must be located in
Code:
<installdir>\Library\RS26\Win32\bpl
otherwise they are not displayed in the "Project Skin Options Editor" inside Rad Studio IDE.

Simply copy them
from: <installdir>\Library\RS26
to: <installdir>\Library\RS26\Win32\bpl