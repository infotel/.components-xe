                                 Using JRZip
                               ===============

JRZip is a Delphi interface to the zlib-library from Jean-Loup Gailly and Mark Adler.
It comes as an unit and has to be included in the uses statement of your program. Further
you should add the appropriate unit search path to your delphi project options.

Actual version: 1.3 (13.11.02)

Features:
---------
  - Packing and unpacking of gz-format (gz-files contain original filename and timestamp)
  - Creating of Zip-compatible archives (including subdirectories)
    (use any standard expander for unzipping)

Routines for gz-format:
-----------------------
  - Create gz-file:     GZip (Source,Destination : string)
  - Unpack gz-file:     GUnzip (Source,Destination : string)
  - Get gz-file info:   GzFileInfo (Source)

Routines for Zip-format:
------------------------
  - Initialize a Zip file:  MakeZip (Destination,BasicSourceDirectory : string)
  - Add file to Zip-file:   AddZip (Source : string)
  - Close Zip-file:         CloseZip


Acknowledgments:
----------------
  - uses a modified version of gzio from Jean-Loup Gailly
    (Pascal translation by Jacques Nomssi Nzali and Francisco Javier Crespo)
  - needs the zlib-library from Jean-Loup Gailly and Mark Adler
    (Pascal translation by Jacques Nomssi Nzali)


List of files
-------------
  JRZip.txt         this file
  JRZip.pas         Delphi unit with the routines described above
  GZioExt.pas       Modified version of gzio.pas

  subdirectory ZLib:
    zconf.inc      Configuration declarations.
    adler.pas      compute the Adler-32 checksum of a data stream
    crc.pas        compute the CRC-32 of a data stream
    gzio.pas       IO on .gz files
    infblock.pas   interpret and process block types to last block
    infcodes.pas   process literals and length/distance pairs
    inffast.pas    process literals and length/distance pairs fast
    inftrees.pas   generate Huffman trees for efficient decoding
    infutil.pas    types and macros common to blocks and codes
    strutils.pas   string utilities
    trees.pas      output deflated data using Huffman coding
    zcompres.pas   compress a memory buffer
    zdeflate.pas   compress data using the deflation algorithm
    zinflate.pas   zlib interface to inflate modules
    zlib.pas       zlib data structures. read the comments there!
    zuncompr.pas   decompress a memory buffer
    zutil.pas


----------------------------------------------------------------------------------------

Legal issues
------------
Copyright (C) 2001 by J. Rathlev

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the author be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.


J. Rathlev
Institut f�r Experimentelle und Angewandte Physik
Universit�t Kiel
Olshausenstr. 40
24098 Kiel
E-Mail: rathlev@physik.uni-kiel.de


