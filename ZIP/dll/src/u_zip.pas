unit u_zip;

interface
uses
  jrzip, SysUtils;

  function zip_File(aSrcFileName: WideString; aDestFileName: WideString): Boolean; stdcall;


exports
  zip_File;

implementation


// ---------------------------------------------------------------
function zip_File(aSrcFileName: WideString; aDestFileName: WideString): Boolean;
// ---------------------------------------------------------------
begin
  Result := False;

  if not FileExists(aSrcFileName) then
  begin
//    ShowMessage('');
    Exit;
  end;

  try
    MakeZip(aDestFileName, ExtractFilePath(aDestFileName));
    AddZip(aSrcFileName);
    CloseZip;

    Result := True;
  except
  end;

end;


end.


//  procedure zip_MakeZip(Destination,BasicDirectory : string); stdcall;

  (* add Source to Pk-Archive *)
//  procedure zip_AddZip(Source : string);  stdcall;

  (* close Pk-Archive, write trailer *)
//  procedure zip_CloseZip; stdcall;



(*  zip_MakeZip,
  zip_AddZip,
  zip_CloseZip;
*)



(*
procedure zip_MakeZip(Destination,BasicDirectory : string);
begin
  jrzip.MakeZip(Destination,BasicDirectory);
end;

procedure zip_AddZip(Source : string);
begin
  jrzip.AddZip(Source);
end;

procedure zip_CloseZip;
begin
  jrzip.CloseZip;
end;
*)
