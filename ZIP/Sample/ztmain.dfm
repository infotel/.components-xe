object Form1: TForm1
  Left = 245
  Top = 107
  Width = 396
  Height = 105
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 105
    Top = 10
    Width = 116
    Height = 36
    Caption = 'Start Zip'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 230
    Top = 10
    Width = 101
    Height = 36
    Caption = 'Ende'
    TabOrder = 1
    OnClick = Button2Click
  end
  object OpenDialog: TOpenDialog
    Filter = 'alle|*.*'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 20
    Top = 30
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'zip'
    Filter = 'Zip-Dateien|*.zip'
    Left = 60
    Top = 30
  end
end
