unit ztmain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, JRZip,
  StdCtrls;

type
  TForm1 = class(TForm)
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    Button1: TButton;
    Button2: TButton;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private-Deklarationen }
    ZName,FDir : string;
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.Button2Click(Sender: TObject);
begin
  close;
  end;

procedure TForm1.Button1Click(Sender: TObject);
var
  i : integer;
begin
  with SaveDialog do if Execute then begin
    ZName:=Filename;
    with OpenDialog do if Execute then begin
      MakeZip (ZName,ExtractFilePath(Filename));
      with Files do for i:=0 to Count-1 do begin
        AddZip (Strings[i]);
        end;
      CloseZip;  
      end;
    end;
  end;

end.
